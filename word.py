from wordfreq import word_frequency
from util import get_result_letter, char_arr_to_str, sigmoid
from functools import lru_cache

class WordData:
    def __init__(self, word, results=None):
        self.word = word
        self.results = results
        self._freq = None
    
    def set_results(results):
        self.results = results
    
    def get_freq(self):
        if not self._freq:
            self._freq = sigmoid(word_frequency(self.word, 'en'), 7.41e-08, 1.66e-06-7.41e-08/6)
        return self._freq
    
    @lru_cache(maxsize=5000000)
    def get_result(self, solution):
        result = ['B','B','B','B','B']
        word_char_list = [c for c in self.word]
        solution_char_list = [c for c in solution]
        length = len(self.word)
        for i in range(length):
            letter = self.word[i]
            if letter == solution[i]:
                result[i] = 'G'
                solution_char_list[solution_char_list.index(letter)] = ""
        for i in range(length):
            letter = self.word[i]
            if letter in solution_char_list and result[i] != "G":
                solution_char_list[solution_char_list.index(letter)] = ""
                result[i] = 'Y'
        return char_arr_to_str(result)
