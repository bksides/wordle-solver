from word import WordData
from util import info, get_entropy

class SolutionSet:
    def __init__(self, result_frequencies):
        self.word_set = {word: WordData(word, results) for word,results in result_frequencies.items()}
        self.words_in_play = set(result_frequencies.keys())

    def entropy(self):
        total_freq = 0
        for word in self.words_in_play:
            total_freq += self.word_set[word].get_freq()
        entropy = 0
        for word in self.words_in_play:
            prob_word = self.word_set[word].get_freq()/total_freq
            entropy += prob_word * info(prob_word)
        return entropy

    def entropy_of_guess(self, guess):
        return get_entropy(self.word_set[guess].results)

    def remove(self, word):
        if word.word not in self.words_in_play:
            return
        for guess in self.word_set:
            result = self.word_set[guess].get_result(word.word)
            self.word_set[guess].results[result] -= word.get_freq()
        self.words_in_play.remove(word.word)

    def reevaluate_result_distributions(self):
        progress=0
        total=len(self.word_set)
        percent = ((progress*1000)//total)/10
        for guess, data in self.word_set.items():
            progress += 1
            old_percent = percent
            percent = ((progress*1000)//total)/10
            if percent > old_percent:
                print("\r{}%".format(percent), end="")
            data.results = {}
            for solution in self.words_in_play:
                result = data.get_result(solution)
                if result in data.results:
                    data.results[data.get_result(solution)] += self.word_set[solution].get_freq()
                else:
                    data.results[data.get_result(solution)] = self.word_set[solution].get_freq()
        print()

    def reduce(self, guess, result):
        print("removing eliminated words from play...")
        self.words_in_play = [word for word in self.words_in_play if self.word_set[guess].get_result(word) == result]
        print("adjusting result distributions...")
        self.reevaluate_result_distributions()
    
    def likeliest_word(self):
        likeliest_word = None
        likeliest_freq = 0
        total_freq = 0
        for solution in self.words_in_play:
            solution_freq = self.word_set[solution].get_freq()
            total_freq += solution_freq
            if solution_freq >= likeliest_freq:
                likeliest_word = solution
                likeliest_freq = solution_freq
        return (likeliest_word, (likeliest_freq*1000/total_freq)//10 if likeliest_word else 0)