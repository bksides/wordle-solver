import util
import json
import argparse
import wordfreq
from word import WordData

def calculate_result_map(word_file_path, answer_file_path, out):
    word_list = util.load_word_list(word_file_path, answer_file_path)
    result_map = {}
    progress=0
    total=len(word_list)
    percent = ((progress*1000)//total)/10
    for word in word_list:
        word_data = WordData(word)
        progress += 1
        new_percent = ((progress*1000)//total)/10
        if new_percent > percent:
            percent = new_percent
            print("\r{}".format(percent), end="")
        result_map[word] = {}
        for solution in word_list:
            solution_data = WordData(solution)
            result = word_data.get_result(solution)
            if result_map[word].get(result):
                result_map[word][result] += solution_data.get_freq()
            else:
                result_map[word][result] = solution_data.get_freq()
    json.dump(result_map, open(out, "w"))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-w", "--word-file", default="./wordle-allowed-guesses.txt", dest="word_list_location")
    parser.add_argument("-a", "--answer-file", default="./wordle-answers-alphabetical.txt", dest="answer_list_location")
    parser.add_argument("-o", "--out", default="./frequencies.txt")
    args = parser.parse_args()
    calculate_result_map(args.word_list_location, args.answer_list_location, args.out)

if __name__ == "__main__":
    main()