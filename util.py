from math import log2, exp

def sigmoid(x, center, stretch):
    x -= 7000
    x /= 500
    return 1 / (1 + exp(-x))

def load_word_list(word_list_location, answer_list_location):
    return [line.strip() for line in open(word_list_location)] + [line.strip() for line in open(answer_list_location)]

def get_entropy(sample_space):
    result_freqs = {}
    total_freq = 0
    for sample, freq in sample_space.items():
        if sample in result_freqs:
            result_freqs[sample] += freq
        else:
            result_freqs[sample] = freq
        total_freq += freq
    result_probabilities = {freq/total_freq for (_, freq) in result_freqs.items()}
    entropy = sum([probability*info(probability) for probability in result_probabilities])
    return entropy

def info(probability):
    return -log2(probability) if 0 < probability < 1 else 0

def get_result_letter(letter, position, solution):
    if solution[position] == letter:
        return "G"
    elif letter in solution:
        return "Y"
    else:
        return "B"

def char_arr_to_str(arr):
    ret = ""
    for x in arr:
        ret += x
    return ret