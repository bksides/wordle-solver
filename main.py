from types import SimpleNamespace
from math import log2
import argparse
from wordfreq import word_frequency
from result_map import calculate_result_map
import json
from functools import cache
from solution_set import SolutionSet
from util import info, load_word_list

def find_best_word(all_words, solution_sets):
    best_word = None
    best_entropy = 0
    progress=0
    total=len(all_words)
    percent = ((progress*1000)//total)/10
    for word in all_words:
        progress += 1
        new_percent = ((progress*1000)//total)/10
        if new_percent > percent:
            percent = new_percent
            print("\r{}%".format(percent), end="")
        entropy = sum([solution_set.entropy_of_guess(word) for solution_set in solution_sets])
        if entropy >= best_entropy:
            best_word = word
            best_entropy = entropy
    return (best_word, best_entropy)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-w", "--word-file", default="./wordle-allowed-guesses.txt", dest="word_list_location")
    parser.add_argument("-a", "--answer-file", default="./wordle-answers-alphabetical.txt", dest="answer_list_location")
    parser.add_argument("-f", "--frequencies-file", default="./frequencies.json")
    parser.add_argument("-p", "--num-puzzles", default=1, type=int)
    args = parser.parse_args()

    result_frequencies = None
    try:
        result_frequencies = json.load(open(args.frequencies_file))
    except(Exception):
        print("Could not find result frequencies file.  Generating... (This only needs to happen once)")
        calculate_result_map(args.word_list_location, args.answer_list_location, args.frequencies_file)
        result_frequencies = json.load(open(args.frequencies_file))

    all_words = [word for word in result_frequencies]
    possible_solution_sets = [SolutionSet(result_frequencies) for _ in range(args.num_puzzles)]
    entropy = sum([solution_set.entropy() for solution_set in possible_solution_sets])
    turn = 1
    while entropy != 0:
        print("\nTURN {}".format(turn))
        turn += 1
        print("Current puzzle entropy: {} bits".format(entropy))
        (best_word, best_word_entropy) = find_best_word(all_words, possible_solution_sets)
        print("\nThe most informative guess would be {} (expecting {} bits)".format(best_word, best_word_entropy))
        guess = input("Enter guess: ").lower()
        # results = []
        for i in range(len(possible_solution_sets)):
            solution_set = possible_solution_sets[i]
            # results.append(input("Enter result {}:".format(i)))
            old_entropy = possible_solution_sets[i].entropy()
            if old_entropy == 0:
                continue
            print("Current individual entropy for subpuzzle {}: {} bits".format(i+1, old_entropy))
            result = input("Enter result {}: ".format(i+1)).upper()
            possible_solution_sets[i].reduce(guess, result)
            new_entropy = possible_solution_sets[i].entropy()
            print("Entropy after reduction: {} bits".format(new_entropy))
            print("Gained {} bits".format(old_entropy-new_entropy))
            if len(possible_solution_sets[i].words_in_play) == 1:
                print("solution {}: {}".format(i+1, possible_solution_sets[i].words_in_play[0]))
            else:
                likeliest_word = possible_solution_sets[i].likeliest_word()
                print("likeliest solution {}: {} ({}%)".format(i+1, likeliest_word[0], likeliest_word[1]))
        old_entropy = entropy
        entropy = sum([solution_set.entropy() for solution_set in possible_solution_sets])
        print("gained {} bits towards solution".format(old_entropy - entropy))
    for i in range(len(possible_solution_sets)):
        solution_set = possible_solution_sets[i]
        if not solution_set:
            print("no solution {}".format(i+1))
        else:
            print("solution {}: {}".format(i+1, solution_set.likeliest_word()))

if __name__=="__main__":
    main()